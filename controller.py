# wrapper to use the wiimote instead of keyboard as controller
# Mirco Piccin, 20 Oct 2018, Nasa Space App Challenges 2018


import cwiid 
import time 
from subprocess import call

#connecting to the Wiimote. This allows several attempts 
# as first few often fail. 
print 'Press 1+2 on your Wiimote now...' 
wm = None 
i=2 

while not wm: 
  try: 
    wm=cwiid.Wiimote() 
  except RuntimeError: 
    if (i>10): 
      quit() 
      break 
    print "Error opening wiimote connection" 
    print "attempt " + str(i) 
    i +=1 

#set Wiimote to report button presses and accelerometer state 
wm.rpt_mode = cwiid.RPT_BTN | cwiid.RPT_ACC 
 
#turn on led to show connected 
wm.led = 1

while True:  
  turn = wm.state['acc'][1]
  if (turn > 140 and turn < 165):
     call(["xdotool","key", "a"])
  elif (turn < 130 and turn > 105):
     call(["xdotool","key", "d"])
     	
  direction = wm.state['acc'][2]
  if (direction > 130 and direction < 150):
     call(["xdotool","key", "w"])
  elif (direction > 100 and direction < 120):
     call(["xdotool","key", "s"])

  #print(wm.state['acc'][2])  
  time.sleep(0.1)


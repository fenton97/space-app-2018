import React from 'react';
import {
  asset,
  View,
  Model,
  Animated,
  StyleSheet
} from 'react-vr';

export default class Rover1 extends React.Component {
  constructor() {
    super();
  }

  render() {

    return (
      <View style={styles.planet}>
        <Animated.View>
          <Model
            source={{
              obj: asset(`models/Rover_final.obj`),
              mtl: asset('models/Rover_final.mtl')
            }}
            lit={true}
            style={[{
              transform: [
              {translate: [ -310 , 1238, 250 ]},
              {scale: 10},
              {rotateX: 0}
            ]}, styles.model]}
          />
        </Animated.View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  planet: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    width: 1400,
    height: 500,
    transform: [{translate: [-700, 0, 0]}]
  },
  model: {
    position: 'absolute'
  }
});
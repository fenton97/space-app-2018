import React from 'react';
import {
  asset,
  Pano,
  View,
  Scene,
  AmbientLight,
  DirectionalLight,
  NativeModules
} from 'react-vr';
import planets from '../data/planets';
import MyHeader from './myHeader';
import Planet from './planet';
import Menu from './menu';
import Info from './info';
import Asteroids from './nativeModules/asteroids';
import Rover from './rover';
import Rover1 from './rover1';
import Rover2 from './rover2';
import Rover3 from './rover3';
import Rover4 from './rover4';
import {VrHeadModel} from 'react-vr';

export default class SolarSystem extends React.Component {

  constructor() {
    super();

    this.state = {
      currentPlanet: 'Mars_2'
    };

    this.x = 0;
    this.y = 0;
    this.z = 0;

    this.speed = 10;
    this.cameraSpeed = 0.04;
    this.viewAngle = 0;

  }

  switchPlanet = (planet) => {
    this.setState({currentPlanet: planet});
  };

  handle(event){
    // console.log('Event',VrHeadModel.rotation()[1]);
    let keyCode = event.nativeEvent.inputEvent.keyCode;

   // X = x + dist * cos(90 - angle)
   // Z = z + dist* sin(90 - angle)
   
    if (keyCode === 65) {
      this.viewAngle = this.viewAngle + this.cameraSpeed;
      NativeModules.TeleportModule.setCamers(this.viewAngle);
    }
    if (keyCode === 68) {
      NativeModules.TeleportModule.setCamers(this.viewAngle);
      this.viewAngle = this.viewAngle - this.cameraSpeed;
    }

    if( Math.abs(this.viewAngle) > 6 ){
      this.viewAngle = 0;
    }

    let headModel = this.viewAngle * 60;
		let angle = Math.abs(90 - Math.abs(headModel)); // get complimentary angle

    if (keyCode === 87) {

      if(headModel < 90) {
        this.z = this.z - (this.speed * Math.sin(angle * Math.PI / 180));
      } else if
      (headModel > 90) {
        this.z = this.z + (this.speed * Math.sin(angle * Math.PI / 180));
      }

      if(headModel > 0) {
        this.x = this.x - (this.speed * Math.cos(Math.abs(angle) * Math.PI / 180));
      }else if(headModel < 0) {
        this.x = this.x + (this.speed * Math.cos(Math.abs(angle) * Math.PI / 180));
      }

    }

    if (keyCode === 83) {
      if(Math.abs(headModel) < 90) {
        this.z = this.z + (this.speed * Math.sin(angle * Math.PI / 180));
      }
      else if(Math.abs(headModel) > 90) {
        this.z = this.z - (this.speed * Math.sin(angle * Math.PI / 180));
      }

      if(headModel > 0) {
        this.x = this.x + (this.speed * Math.cos(Math.abs(angle) * Math.PI / 180));
      }

      else if(headModel < 0) {
        this.x = this.x - (this.speed * Math.cos(Math.abs(angle) * Math.PI / 180));
      }
    }

    NativeModules.TeleportModule.teleportCamera(this.x, this.y, this.z);
    
   
  }

  render() {

    const {
      currentPlanet
    } = this.state;

    return (
          <View onInput={e => this.handle(e)}>
          <Scene style={{ transform: [ {translate: [ -200 ,40, 800]} ] }}/>
  
          <Pano source={asset('stars.jpg')}/>
  
          <AmbientLight intensity={0.5}/>
          <DirectionalLight intensity={0.5}/>
  
          <MyHeader currentPlanet={currentPlanet} />
  
          <Planet currentPlanet={currentPlanet}/>
          
          <Rover/>
          <Rover1/>
          <Rover2/>
          <Rover3/>
          <Rover4/>
          
          {/* <Menu
            planets={planets}
            switchPlanet={this.switchPlanet}
            currentPlanet={currentPlanet} /> */}
  
          {/* <Info currentPlanet={currentPlanet} /> */}
  
          {/* <Asteroids /> */}
  
        </View>
    );
  }
};
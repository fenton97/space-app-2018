import React from 'react';
import {
  Text,
  View,
  StyleSheet,
  asset,
  Image
} from 'react-vr';

// import Image from 'react-native';

const MyHeader = ({currentPlanet}) => (
  <View style={styles.header}>
    {/* <Text style={styles.headerTitle}>The Solar System</Text> */}
    <Image style={styles.image} source={asset('logo.png')}/>
    {/* <Text style={styles.headerSubtitle}>{currentPlanet}</Text> */}
  </View>
);

const styles = StyleSheet.create({
  header: {
    position: 'absolute',
    width: 600,
    transform: [{translate: [-300, 180, 0]}]
  },
  headerTitle: {
    fontSize: 60,
    textAlign: 'center',
    color: '#fff'
  },
  headerSubtitle: {
    fontSize: 30,
    textAlign: 'center',
    color: '#fff'
  },
  image:{
    position: 'absolute',
    height: 150,
    width: 200
  }
});

export default MyHeader;
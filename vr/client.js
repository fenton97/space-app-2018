// Auto-generated content.
// This file contains the boilerplate to set up your React app.
// If you want to modify your application, start in "index.vr.js"

// Auto-generated content.
import {VRInstance, Module} from 'react-vr-web';

// for the native modules
import AsteroidsModule from '../components/nativeModules/asteroidsModule';
import * as THREE from 'three';

class TeleportModule extends Module {
  constructor() {
    super('TeleportModule');
    this._camera = null;
  }

  setCamera(camera) {
    this._camera = camera;
  }

  teleportCamera(x, y, z) {
    if (this._camera) {
      this._camera.position.set(x, y, z);
      // Call this to make sure anything positioned relative to the camera is set up properly:
      this._camera.updateMatrixWorld(true);
    }
  }

  setCamers(rotation){    
    this._camera.rotation.y = rotation;
  }

}

function init(bundle, parent, options) {

  // new scene and nativeModules instances
  const scene = new THREE.Scene();
  const teleportModule = new TeleportModule();

  const vr = new VRInstance(bundle, 'solarSystem', parent, {
    // Add custom options here
    nativeModules: [ teleportModule ],
    scene,
    ...options,
  });

  const nonVRControls = vr.player.controls.nonVRControls;
    if (!(nonVRControls instanceof require('ovrui').DeviceOrientationControls)) {
      nonVRControls.disconnect();
  }

  teleportModule.setCamera(vr.player.camera);

  vr.render = function() {
    // Any custom behavior you want to perform on each frame goes here

  };

  // Begin the animation loop
  vr.start();
  return vr;
}

window.ReactVR = {init};
